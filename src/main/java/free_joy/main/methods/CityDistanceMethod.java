package free_joy.main.methods;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

public class CityDistanceMethod implements TemplateMethodModelEx
{
	DecimalFormat format = new DecimalFormat("###,###", new DecimalFormatSymbols(Locale.GERMAN));
	
	@Override
	@SuppressWarnings("rawtypes")
	public Object exec(List arguments) throws TemplateModelException
	{
		if(arguments.size() != 4) throw new TemplateModelException("Argument amount ["+arguments.size()+"] is supposed to be 4");
		SimpleNumber lat1 = (SimpleNumber)arguments.get(0);
		SimpleNumber lat2 = (SimpleNumber)arguments.get(2);
		SimpleNumber lon1 = (SimpleNumber)arguments.get(1);
		SimpleNumber lon2 = (SimpleNumber)arguments.get(3);
		int distance = (int)distance(lat1.getAsNumber(), lat2.getAsNumber(), lon1.getAsNumber(), lon2.getAsNumber());
		return new SimpleScalar(format.format(distance/1000)+"km");
	}
	
	public static double distance(Number lat1, Number lat2, Number lon1, Number lon2)
	{
		return distance(lat1.doubleValue(), lat2.doubleValue(), lon1.doubleValue(), lon2.doubleValue(), 0D, 0D);
	}
	
	public static double distance(double lat1, double lat2, double lon1, double lon2, double el1, double el2) 
	{
	    final int R = 6371; // Radius of the earth
	    double latDistance = Math.toRadians(lat2 - lat1);
	    double lonDistance = Math.toRadians(lon2 - lon1);
	    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)  + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    double distance = R * c * 1000; // convert to meters
	    double height = el1 - el2;
	    distance = Math.pow(distance, 2) + Math.pow(height, 2);
	    return Math.sqrt(distance);
	}
}