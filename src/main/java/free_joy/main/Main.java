package free_joy.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import free_joy.main.apdapters.CityObjectWrapper;
import free_joy.main.helpers.City;
import free_joy.main.helpers.CityCompare;
import free_joy.main.methods.CityDistanceMethod;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import it.unimi.dsi.fastutil.objects.Object2ObjectLinkedOpenHashMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class Main
{
	Configuration config;
	
	public static void main(String...args) throws IOException
	{
        new Main().run();
	}
	
	private void run()
	{
		if(init())
		{
			process(initData());
			cleanup();
		}
	}
	
	private boolean init()
	{
		try
		{
	        Configuration cfg = new Configuration(Configuration.VERSION_2_3_31);
	        cfg.setDirectoryForTemplateLoading(new File("src/main/resources/templates"));
	        cfg.setDefaultEncoding("UTF-8");	
	        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
	        cfg.setLogTemplateExceptions(false);
	        cfg.setWrapUncheckedExceptions(true);
	        cfg.setFallbackOnNullLoopVariable(false);
	        config = cfg;		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private Map<String, Object> initData()
	{
		Map<String, Object> result = new Object2ObjectLinkedOpenHashMap<>();
		List<City> list = new ObjectArrayList<>();
		list.add(new City("Washington", "en_US", 7796940, 47.751076, -120.740135));
		list.add(new City("Berlin", "de_DE", 3566791, 52.520008, 13.404954));
		list.add(new City("Sidney", "en_AU", 4992000, -33.865143, 151.209900));
		List<CityCompare> compare = new ObjectArrayList<>();
		for(City mainCity : list)
		{
			for(City subCity : list)
			{
				if(mainCity == subCity) continue;
				CityCompare city = new CityCompare(mainCity, subCity);
				if(compare.contains(city)) continue;
				compare.add(city);
			}
		}
		result.put("cities", list);
		result.put("city_compare", compare);
		result.put("city_distance", new CityDistanceMethod());
		return result;
	}
	
	private void process(Map<String, Object> data)
	{
		config.setObjectWrapper(new CityObjectWrapper(Configuration.VERSION_2_3_31));
		process("cities.ftl", "index.html", data);
	}
	
	private void process(String input, String output, Map<String, Object> data)
	{
		try
		{
			Template template = config.getTemplate(input);
			if(template == null) return;
			BufferedWriter writer = new BufferedWriter(new FileWriter("output/"+output));
			template.process(data, writer);
			writer.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
		System.out.println("Processed ["+input+"] into ["+output+"]");
	}
	
	private void cleanup()
	{
		config = null;
	}
	
}
