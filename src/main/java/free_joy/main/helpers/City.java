package free_joy.main.helpers;

public class City
{
	String name;
	String langCode;
	int population;
	double latitude;
	double longitude;
	
	public City(String name, String langCode, int population, double latitude, double longitude)
	{
		this.name = name;
		this.langCode = langCode;
		this.population = population;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public String getLocale()
	{
		return langCode;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getPopulation()
	{
		return population;
	}
	
	public double getLatitude()
	{
		return latitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
}
