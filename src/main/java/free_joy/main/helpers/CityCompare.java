package free_joy.main.helpers;

import java.util.Objects;

public class CityCompare
{
	private City mainCity;
	private City subCity;
	
	public CityCompare(City mainCity, City subCity)
	{
		this.mainCity = mainCity;
		this.subCity = subCity;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == this) return true;
		if(obj instanceof CityCompare)
		{
			CityCompare other = (CityCompare)obj;
			return (other.mainCity == mainCity && other.subCity == subCity) || (other.subCity == mainCity && other.mainCity == subCity);
		}
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return Objects.hash(mainCity.getName(), subCity.getName());
	}
	
	public City getMainCity()
	{
		return mainCity;
	}
	
	public City getSubCity()
	{
		return subCity;
	}
}
