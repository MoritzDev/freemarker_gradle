package free_joy.main.apdapters;

import java.util.Map;
import java.util.function.BiFunction;

import free_joy.main.helpers.City;
import free_joy.main.helpers.CityCompare;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.Version;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;

public class CityObjectWrapper extends DefaultObjectWrapper
{
	Map<Class<?>, BiFunction<? super Object, ObjectWrapper, TemplateModel>> functions = new Object2ObjectOpenHashMap<>();
	
	public CityObjectWrapper(Version version)
	{
		super(version);
		register(City.class, CityAdapter::new);
		register(CityCompare.class, CityCompareAdapter::new);
	}
	
	@SuppressWarnings("unchecked")
	private <T extends Object> void register(Class<T> clz, BiFunction<T, ObjectWrapper, TemplateModel> function)
	{
		functions.put(clz, (BiFunction<? super Object, ObjectWrapper, TemplateModel>)function);
	}
	
	@Override
	protected TemplateModel handleUnknownType(Object obj) throws TemplateModelException
	{
		BiFunction<? super Object, ObjectWrapper, TemplateModel> builder = functions.get(obj.getClass());
		if(builder != null) return builder.apply(obj, this);
		return super.handleUnknownType(obj);
	}
}
