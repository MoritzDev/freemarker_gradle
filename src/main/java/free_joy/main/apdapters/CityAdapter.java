package free_joy.main.apdapters;

import free_joy.main.helpers.City;
import freemarker.template.AdapterTemplateModel;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;
import freemarker.template.WrappingTemplateModel;

public class CityAdapter extends WrappingTemplateModel implements TemplateSequenceModel, AdapterTemplateModel
{
	City city;
	
	public CityAdapter(City city, ObjectWrapper wrapper)
	{
		super(wrapper);
		this.city = city;
	}

	@Override
	public Object getAdaptedObject(Class<?> hint)
	{
		return city;
	}
	
	@Override
	public TemplateModel get(int index) throws TemplateModelException
	{
		switch(index) 
		{
			case 0: return wrap(city.getName());
			case 1: return wrap(city.getPopulation());
			case 2: return wrap(city.getLocale());
			case 3: return wrap(city.getLatitude());
			case 4: return wrap(city.getLongitude());
			default: return wrap("I AM ERROR");
		}
	}
	
	@Override
	public int size() throws TemplateModelException
	{
		return 5;
	}
	
}
