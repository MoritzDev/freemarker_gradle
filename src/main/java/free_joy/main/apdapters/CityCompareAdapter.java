package free_joy.main.apdapters;

import free_joy.main.helpers.CityCompare;
import freemarker.template.AdapterTemplateModel;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateSequenceModel;
import freemarker.template.WrappingTemplateModel;

public class CityCompareAdapter extends WrappingTemplateModel implements TemplateSequenceModel, AdapterTemplateModel
{
	CityCompare compare;
	
	public CityCompareAdapter(CityCompare compare, ObjectWrapper wrapper)
	{
		super(wrapper);
		this.compare = compare;
	}

	@Override
	public Object getAdaptedObject(Class<?> hint)
	{
		return compare;
	}

	@Override
	public TemplateModel get(int index) throws TemplateModelException
	{
		switch(index) 
		{
			case 0: return wrap(compare.getMainCity());
			case 1: return wrap(compare.getSubCity());
			default: return wrap("I AM ERROR");
		}
	}

	@Override
	public int size() throws TemplateModelException
	{
		return 2;
	}
	
}
