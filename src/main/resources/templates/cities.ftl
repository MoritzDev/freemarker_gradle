<html lang="en">
<head>
    <title>Freemarker Population Page</title>
</head>
<body>
<h1>Population Page</h1>
<p>This Page is allowing you to see very specific Cities, their Lang Code and how many people live at any given city</p>
<a>Cities:</a>

<ul>
<#list cities as city>
	<li><b>${city[0]}</b>, Population: ${city[1]}, Locale Code: ${city[2]} </li>
</#list>
</ul>

<a>Distance between cities</a>
<ul>
<#list city_compare as city>
	<li>Distance between <b>${city[0][0]}</b> and <b>${city[1][0]}</b> is ${city_distance(city[0][3], city[1][3], city[0][4], city[1][4])}</li>
</#list>
</ul>

</body>
</html>
